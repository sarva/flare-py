import networkx as nx
import numpy as np

nsamp = 10
N = 2000
ws_neighbors = 4
rewire_prob = 0.3
radius = 2
num_iterations = 100

reachability_averages_q0 = []
reachability_averages_q1 = []

for it in range(num_iterations):
    G = nx.watts_strogatz_graph(N, ws_neighbors, rewire_prob)
    reachable_node_counts_q0 = []
    reachable_node_counts_q1 = []
    sample_nodes = np.random.randint(0, N-1, nsamp).tolist()
    for i in sample_nodes:
        # Nodes within neighborhood radius
        # We deduct one to exclude the source node itself from the reachable node count
        reachable_node_counts_q0.append(len(nx.single_source_shortest_path(G, i, radius))-1)

        # Nodes reachable after one table request. This includes the nodes in the above list as well.
        reachable_node_counts_q1.append(len(nx.single_source_shortest_path(G, i, 2*radius))-1)

    avg_q0 = np.average(reachable_node_counts_q0)
    avg_q1 = np.average(reachable_node_counts_q1) - avg_q0
    print avg_q0, avg_q1
    reachability_averages_q0.append(avg_q0)
    reachability_averages_q1.append(avg_q1)

print 'Average reachability with Q = 0 is ', np.average(reachability_averages_q0)
print 'Average reachability percentage with Q = 0 is ', 100*np.average(reachability_averages_q0)/N

print 'Average reachability with Q = 1 is ', np.average(reachability_averages_q1)
print 'Average reachability percentage with Q = 1 is ', 100*np.average(reachability_averages_q1)/N
