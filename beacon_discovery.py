import networkx as nx
import numpy as np

nb = 4
N = 2000
ws_neighbors = 4
rewire_prob = 0.3
radius = 2
num_iterations = 10

def generate_node_addresses(num_nodes):
    addresses = []
    for i in range(num_nodes):
        a = np.random.bytes(8) # 64-bit addresses
        a_int = int(a.encode('hex'), 16)
        addresses.append(a_int)
    return addresses

def xor_dist(a, b):
    return a | b

def node_routing_table(G, i):
    neighbor_paths = nx.single_source_shortest_path(G, i, radius)
    k = dict.keys(neighbor_paths)
    return G.subgraph(k)

def find_closest_address_space_node(dst, nodelist, addr_list):
    node_dist_tuples = []
    for x in nodelist:
        node_dist_tuples.append( (x, xor_dist(addr_list[dst], addr_list[x])) )
    slist = sorted(node_dist_tuples, key= lambda (x, distx): distx)
    (closest_node, dist) = slist[0]
    return closest_node

def send_beacon_request(beacon_routing_table,addr_list,src,path_to_beacon,beacon):
    beacon_neighbors = list(beacon_routing_table.nodes())
    beacon_neighbors.remove(beacon)
    if src in beacon_neighbors: beacon_neighbors.remove(src)
    beacon_candidates = []
    path_to_beacon.pop()
    for x in beacon_neighbors:
        if(xor_dist(addr_list[x],addr_list[src]) < xor_dist(addr_list[beacon],addr_list[src])):
            path_from_beacon = nx.shortest_path(beacon_routing_table,beacon,x)
            path_to_x = path_to_beacon + path_from_beacon
            beacon_candidates.append((x,path_to_x))
    return beacon_candidates

def select_beacons(addr_list,src,responsive_nodes):
 	node_list = [x[0] for x in responsive_nodes]
 	node_dist_tuples = []
	for n in node_list:
		node_dist_tuples.append( (n, xor_dist(addr_list[src], addr_list[n])) )
	slist = sorted(node_dist_tuples, key= lambda (x, distx): distx)
	del slist[nb:]
	return slist

# Main program logic begins

for it in range(num_iterations):
    G = nx.watts_strogatz_graph(N, ws_neighbors, rewire_prob)
    Gaddr = generate_node_addresses(N)
    for src in range(N):
    	beacons = []
        unprocessed_beacon_candidates = []
        src_routing_table = node_routing_table(G,src)
        src_neighbors = list(src_routing_table.nodes())
        src_neighbors.remove(src)
        neighbor_dist_tuples = []
        for i in src_neighbors:
                neighbor_dist_tuples.append((i,xor_dist(Gaddr[src], Gaddr[i])))
        neighbor_sorted_list = sorted(neighbor_dist_tuples, key= lambda (x, distx): distx)
        del neighbor_sorted_list[nb:]
        initial_candidates = [x[0] for x in neighbor_sorted_list]
        for n in initial_candidates:
            unprocessed_beacon_candidates.append((n,nx.shortest_path(src_routing_table,src,n)))
        processed_nodes = []
        responsive_nodes = []
        while (unprocessed_beacon_candidates != []):
            candidate_nodes = [x[0] for x in unprocessed_beacon_candidates]
            closest_node = find_closest_address_space_node(src,candidate_nodes,Gaddr)
            closest_node_index = candidate_nodes.index(closest_node)
            path_to_closest_node = unprocessed_beacon_candidates[closest_node_index][1] 
            unprocessed_beacon_candidates.pop(closest_node_index)
            processed_nodes.append(closest_node)
            responsive_nodes.append((closest_node,path_to_closest_node))
            closest_node_routing_table = node_routing_table(G,closest_node)
            unprocessed_beacon_candidates = unprocessed_beacon_candidates + send_beacon_request(closest_node_routing_table,Gaddr,src,path_to_closest_node,closest_node)
            unprocessed_beacon_candidates = [(x,y) for (x,y) in unprocessed_beacon_candidates if x not in processed_nodes]
            unprocessed_beacon_candidates = list(dict(unprocessed_beacon_candidates).items())
            if (len(unprocessed_beacon_candidates)> nb):
                unprocessed_beacon_candidates = sorted(unprocessed_beacon_candidates, key= lambda (x, pathx): -len(pathx))
                del unprocessed_beacon_candidates[nb:]   

    	beacons = select_beacons(Gaddr,src,responsive_nodes)
    	#TODO  add beacons to graph
    	beaconnodes = [x[0] for x in beacons]
   
        #calculate closest nodes
        node_dist_tuples = []
        for i in range(N):
        	if (i != src):
        		node_dist_tuples.append((i,xor_dist(Gaddr[src], Gaddr[i])))
        ranklist = sorted(node_dist_tuples, key= lambda (x, distx): distx)
        closest_node_list = [x[0] for x in ranklist]
        beacon_rank_tuples = []
        for b in beaconnodes:
            beacon_rank = closest_node_list.index(b)+1
            beacon_rank_tuples.append((b,beacon_rank))
        print "Beacon ranks for source" , src , "for iteration" , it , "is" , beacon_rank_tuples
    
   

                        



