import networkx as nx
import numpy as np

nsamp = 10
N = 2000
ws_neighbors = 4
rewire_prob = 0.3
radius = 2
num_iterations = 100
num_table_queries = 9 # Query to destination is not included in this number

def generate_node_addresses(num_nodes):
    addresses = []
    for i in range(num_nodes):
        a = np.random.bytes(8) # 64-bit addresses
        a_int = int(a.encode('hex'), 16)
        addresses.append(a_int)
    return addresses

def xor_dist(a, b):
    return a | b

def node_routing_table(G, i):
    neighbor_paths = nx.single_source_shortest_path(G, i, radius)
    k = dict.keys(neighbor_paths)
    return G.subgraph(k)

def find_closest_address_space_node(dst, nodelist, addr_list):
    node_dist_tuples = []
    for x in nodelist:
        node_dist_tuples.append( (x, xor_dist(addr_list[dst], addr_list[x])) )
    slist = sorted(node_dist_tuples, key= lambda (x, distx): distx)
    (closest_node, dist) = slist[0]
    return closest_node

def is_reachable(G, addr_list, src, dst, ntab):
    src_routing_table = node_routing_table(G, src)
    dst_routing_table = node_routing_table(G, dst)
    requested_node_list = []
    for i in range(ntab):
        src_neighbors = list(src_routing_table.nodes())
        src_neighbors.remove(src)
        for r in requested_node_list:
            src_neighbors.remove(r)
        requested_node = find_closest_address_space_node(dst, src_neighbors, addr_list)
        requested_node_routing_table = node_routing_table(G, requested_node)
        requested_node_list.append(requested_node)
        src_routing_table = G.subgraph(list(src_routing_table.nodes()) + list(requested_node_routing_table.nodes()))
        for dst_neighbor in list(dst_routing_table.nodes()):
            if src_routing_table.has_node(dst_neighbor):
                return True
    return False

# Main program logic begins
reachability_averages_q2 = []

for it in range(num_iterations):
    G = nx.watts_strogatz_graph(N, ws_neighbors, rewire_prob)
    Gaddr = generate_node_addresses(N)
    reachable_node_counts_q2 = []

    sample_nodes = np.random.randint(0, N-1, nsamp).tolist()
    for i in sample_nodes:
        count_q2 = 0
        for j in range(N):
            # If shortest path length is <= 4, then one table request
            # is enough to reach the destination j
            if j != i and nx.shortest_path_length(G, i, j) > 4:
                if is_reachable(G, Gaddr, i, j, num_table_queries):
                    count_q2 += 1

        reachable_node_counts_q2.append(count_q2)
        print 'Iteration', it+1, ': Q >= 2 reachability for node', i, 'is', count_q2, 'nodes out of', N
    avg_q2 = np.average(reachable_node_counts_q2)
    reachability_averages_q2.append(avg_q2)
    
    print 'Q >= 2 reachability of iteration', it, 'is', 100*avg_q2/N
                    
print 'Average reachability percentage with Q >= 2 is ', 100*np.average(reachability_averages_q2)/N
                        



